function renderDSSV(dssv){
    contentHTML = "";
    for (var i = 0; i < dssv.length; i++) {
      var sv = dssv[i];
      var content = `<tr>
      <td>${sv.ma}</td>
      <td>${sv.ten}</td>
      <td>${sv.matKhau}</td>
      <td>${sv.tinhDTB()}</td>
      <td><button onclick="xoaSinhVien('${sv.ma}')" class="btn btn-danger">Xoá</button>
      <button class="btn btn-warning" onclick="suaSinhVien('${sv.ma}')">Sửa</button></td>
      </tr>`;
      contentHTML +=content;
    };
    document.getElementById("tbodySinhVien").innerHTML = contentHTML; 
}
 
function showThongTinLenForm(sv){
     document.getElementById("txtMaSV").value=sv.ma;
     document.getElementById("txtTenSV").value=sv.ten;
     document.getElementById("txtEmail").value=sv.email;
     document.getElementById("txtPass").value=sv.matKhau;
     document.getElementById("txtDiemToan").value=sv.toan;
     document.getElementById("txtDiemLy").value=sv.ly;
    document.getElementById("txtDiemHoa").value=sv.hoa;
}

function layThongTinTuForm(){
    //Get data from HTML tag
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = Number(document.getElementById("txtDiemToan").value);
  var ly = Number(document.getElementById("txtDiemLy").value);
  var hoa = Number(document.getElementById("txtDiemHoa").value);
  //Create object SV
  return new SinhVien(ma,ten,email,matKhau,toan,ly,hoa);
}
