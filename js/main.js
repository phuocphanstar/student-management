//Input student data
var dssv = [];
//Get data from local storage and render
var dataJSON=localStorage.getItem("DSSV");
if (dataJSON!=null){
    dssv=JSON.parse(dataJSON).map(function(item){ //array map
        return new SinhVien(item.ma,item.ten,item.email,item.matKhau,item.toan,item.ly,item.hoa);
    });
    renderDSSV(dssv);
}

function themSinhVien() {
  var sv= layThongTinTuForm();
  //validate phần mã
  var isValid= kiemTraTrung(sv.ma)&&kiemTraDoDai("spanMaSV","Mã chỉ gồm 5 kí tự",5,5,sv.ma);
  //validate phần mật khẩu
  isValid =isValid&kiemTraDoDai("spanMatKhau","Mật khẩu gồm 10 kí tự",10,10,sv.matKhau);
  //validate phần email
  isValid=isValid&kiemTraEmail("spanEmailSV",sv.email);
if(isValid){
  dssv.push(sv);
  renderDSSV(dssv);
  //Save DSSV into local storage
  var dataJSON =JSON.stringify(dssv);
  localStorage.setItem("DSSV",dataJSON);

}
}
function xoaSinhVien(id){
    var index=dssv.findIndex(function (item){
        return item.ma==id;
    });
    dssv.splice(index,1); //splice cut object with position index, 1 step.
    renderDSSV(dssv);
    var dataJSON =JSON.stringify(dssv);
    localStorage.setItem("DSSV",dataJSON);
}
function suaSinhVien(id){
    var index=dssv.findIndex(function(item){
        return item.ma==id;
    });
    showThongTinLenForm(dssv[index]);
}
function capNhatSinhVien(){
    var sv = layThongTinTuForm();
    var index =dssv.findIndex(function (item){
        return item.ma == sv.ma;
    });
    dssv[index]=sv;
    renderDSSV(dssv);
}
function reSetForm(){
    document.getElementById("formQLSV").reset();
}